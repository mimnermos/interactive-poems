## interactive-poems

template for responsive canvas by https://github.com/SableRaf/HicEtNunc-p5js-templates 

The poem with its subpoems has to be specified inside the poems object.

Inside the p5js draw() function, the call writeMainPoem() reads the poems object and draws the main poem with the clickable words, which are also pushed to the clickableWords array.

Depending on last left click mouse position, the corresponding sub-poem from the poems object is then displayed.

All the common styling should go into the draw() function.

The constants TEXT_CLICKABLE_COLOR and TEXT_COLOR determine respectively a clickable word font color and a normal word font color.

Constants WORD_WIDTH_SCALING_FACTOR and WIRD_HEIGHT_SCALING_FACTOR are used during calculation of word positions.

A monospaced font is recommended, otherwise calculations will be all messed up.
