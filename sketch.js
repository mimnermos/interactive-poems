// **************************
// *       PARAMETERS       *
// **************************

// Set this to true when minting
p5.disableFriendlyErrors = false;

// The title of your piece goes here (not visible on hicetnunc)
document.title = "My beautiful p5.js sketch";

// Default size of your canvas (windowScale = 1.0)
const referenceSize = 600;

// if true, then the canvas cannot be larger than the reference size
const hasMaxSize = false;

// if true the canvas will be vertically and horizontally centered inside the window
const isCentered = true;

// **************************
// *    GLOBAL VARIABLES    *
// **************************

var canvasSize;
var windowScale;
var fullScreen;
var fontSize;

var lastX;
var lastY;

// true if mouse was pressed in previous frame
var pressed = false;
// true if we're not displaying the main poem, i.e. after a click
var subpoem = false;

let TEXT_CLICKABLE_COLOR; 
let TEXT_COLOR;

const WORD_WIDTH_SCALING_FACTOR = 2;
const WORD_HEIGHT_SCALING_FACTOR = 1.6;

// **************************
// *        PRELOAD         *
// **************************

function preload() {}

// **************************
// *          SETUP         *
// **************************

function setup() {
  colorMode(HSL);
  TEXT_CLICKABLE_COLOR = color(180, 100, 50);
  TEXT_COLOR = color(0, 0, 0);
  smooth();
  setDimensions();
  if (isCentered) {
    centerCanvas();
  }
  createCanvas(canvasSize, canvasSize);
  lastX = mouseX;
  lastY = mouseY;
}

// **************************
// *          DRAW          *
// **************************

function getHeight(verseNumber){
  return ((verseNumber-1)*3) * fontSize/WORD_HEIGHT_SCALING_FACTOR;
}

// array of { words: words array, i: number of word in array, w: width, h: height, c: callback }
var clickableWords = [];
var clickablePhases = [];

function writeVerse(verse, height, cw, callbacks) {
  var words = verse.split(" ");
  const f = num => num === i;
  for (var i = 0; i < words.length; i++){
    const isClickable = !!cw && !!cw.filter(f).length;
    if(!!isClickable){
      var b = brightness(TEXT_CLICKABLE_COLOR);
      // we have yet to fill clickableWords with this clickable word, so the index of this phase is just the length
      b += (sin(frameCount*0.1) * b) + clickablePhases[clickableWords.length];
      fill(hue(TEXT_CLICKABLE_COLOR), saturation(TEXT_CLICKABLE_COLOR), b);
      //fill(TEXT_CLICKABLE_COLOR);
      writeClickableWord(words, i, height, callbacks.shift());
    }else{
      fill(TEXT_COLOR);
      writeWord(words, i, height);
    }
  }
}
 
function getWordWidth(word){
  return word.length * fontSize/WORD_WIDTH_SCALING_FACTOR
}

function getWidth(words, wordNumber){
  var width = 0;
  for (var i = 0; i < wordNumber; i++){
    width += getWordWidth(words[i]);
    width += fontSize;
  }
  return width;
}

function writeWord(words, wordNumber, height) {
  var wordToWrite = words[wordNumber];
  var width = getWidth(words, wordNumber);
  text(wordToWrite, width, height);
  return {w: width, h: height};
}

function writeClickableWord(words, wordNumber, height, callback) {
  var res = writeWord(words, wordNumber, height);
  clickableWords.push({words: words, i: wordNumber, w: res.w, h: res.h, c: callback});
}

function writeMainPoem() {
    const fun = (w) => {
        for (var j=0; j<poems[w].length;j++){
          writeVerse(poems[w][j], getHeight(j+1), [], []);
        }
        
      };
    for (var i = 0; i< poems.mainPoem.length; i++){
      const words = poems.mainPoem[i].split(" ");
      const clickable = words
        .filter(word => poems.hasOwnProperty(word));
      const clickableNumber = clickable.map(w => words.indexOf(w));
      writeVerse(poems.mainPoem[i],
                 getHeight(i+1), 
                 clickableNumber,
                 clickableNumber.map(k => () => fun(words[k]))
      );
    }
}

const poems = {
  mainPoem: [
              "first poem line",
              "second poem line",
              "third line"
            ],
  poem: ["OK"],
  second: ["yes it does work", "indeed"],
  third: ["oh no"]
}

function draw() {
  background(color(25, 25, 25));

  var strokeW = 10 * windowScale; // the stroke will be 10 px when canvasSize == referenceSize
  var diameter = 0.8 * canvasSize; // here the circle's diameter is 80% of the canvas size

  var transX = canvasSize / 6;
  var transY = canvasSize / 8;
  // draw the content
  translate(transX, transY);
  textSize(fontSize);
  textAlign(LEFT);
  textFont("Monospace");

  const clickFilter = (el) => (el.w+transX) < lastX && lastX <  (el.w+getWordWidth(el.words[el.i])+transX+fontSize) && (el.h+transY-fontSize/WORD_HEIGHT_SCALING_FACTOR) < lastY && lastY < (el.h + transY +fontSize/WORD_HEIGHT_SCALING_FACTOR);
  const filtered = clickableWords.filter(clickFilter);
  
  if(filtered.length > 0){
    // should be just one
    filtered[0].c();
    subpoem = true;
  }else{
    clickableWords = [];
    writeMainPoem();
    subpoem = false;
  }

  clickablePhases = [];
  clickableWords.forEach(w => clickablePhases.push(random(0, 128)));
  
  fill(220, 10, 150);
  if (fullScreen) fill(20, 200, 150);

  // Create a screen reader accessible description for the canvas
  describe(
    "poem text?"
  );
}

function mouseClicked() {
  if(subpoem){
    clickableWords = []; 
    lastX = 0;
    lastY = 0;
  }else{
    lastX = mouseX;
    lastY = mouseY;
  }
}

// **************************
// *        RESIZE          *
// **************************

function windowResized() {
  setDimensions();
  resizeCanvas(canvasSize, canvasSize);
}

// **************************
// *         INPUT          *
// **************************

// Find key codes at http://keycode.info/
function keyPressed() {
  if (keyCode === 82) {
    console.log("R was pressed");
  } else if (keyCode === 67) {
    console.log("C was pressed");
  }
}

// **************************
// *         UTILS          *
// **************************

function setDimensions() {
  fullScreen = isFullscreen();

  // This is how we constrain the canvas to the smallest dimension of the window
  // Thanks to Maxim Schoemaker for this trick! twitter.com/MaximSchoemaker - maximschoemaker.com
  canvasSize = min(windowWidth, windowHeight);

  if (hasMaxSize) {
    canvasSize = min(referenceSize, canvasSize);
  }

  // windowScale goes from 0.0 to 1.0 as canvasSize goes from 0.0 to referenceSize
  // if hasMaxSize is set to true, it will be clamped to 1.0 otherwise it keeps growing over 1.0
  windowScale = map(canvasSize, 0, referenceSize, 0, 1, hasMaxSize);
  
   fontSize = 20 * windowScale;
}

function centerCanvas() {
  var s = document.body.style;
  s.display = "flex";
  s.overflow = "hidden";
  s.height = "100vh";
  s.alignItems = "center";
  s.justifyContent = "center";
}

function isFullscreen() {
  if (
    document.fullscreenElement ||
    window.screen.height - window.innerHeight <= 3 ||
    isEdgeFullscreen() ||
    isSafariFullscreen()
  ) {
    return true;
  }
  return false;
}

function isSafariFullscreen() {
  if (document.webkitIsFullScreen) {
    return true;
  }
  return false;
}

function isEdgeFullscreen() {
  if (isUserAgent("Edg") && window.screen.height - window.innerHeight <= 235) {
    return true;
  }
  return false;
}

function isUserAgent(name) {
  if (window.navigator.userAgent.indexOf(name) > -1) {
    return true;
  }
  return false;
}

// toggle fullscreen (for testing)
// function mousePressed() {
//   let fs = fullscreen();
//   fullscreen(!fs);
// }

